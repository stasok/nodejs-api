import express from 'express'
import fetch from 'isomorphic-fetch'
import Promise from 'bluebird'
import _ from 'lodash'

import canonize from './canonize'
import isNumeric from './sum'
import getShortFullName from './fio'


const __DEV__ = true;
const app = express();


app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});





const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

/*
 {
 "board":{
 "vendor":"IBM",
 "model":"IBM-PC S-100",
 "cpu":{
 "model":"80286",
 "hz":12000
 },
 "image":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg",
 "video":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"
 },
 "ram":{
 "vendor":"CTS",
 "volume":1048576,
 "pins":30
 },
 "os":"MS-DOS 1.25",
 "floppy":0,
 "hdd":[
 {
 "vendor":"Samsung",
 "size":33554432,
 "volume":"C:"
 },{
 "vendor":"Maxtor",
 "size":16777216,
 "volume":"D:"
 },{
 "vendor":"Maxtor",
 "size":8388608,
 "volume":"C:"
 }
 ],
 "monitor":null
 }
 */

let pc = {};
console.log('Fetching JSON');
fetch(pcUrl)
	.then(async (res) => {
		console.log('Got it!');
		pc = await res.json();
		console.log(pc);
	})
	.catch(err => {
		console.log('Чтото пошло не так:', err);
	});


function notFound(res) {
	res.status(404).send('Not Found');
}

app.get('/task3A', (req, res) => {
	res.json(pc);
});


app.get('/task3A/volumes', (req, res) => {
	if (pc.hdd) {
		const vols = {};
		pc.hdd.map( (item) => {
			if (vols[item.volume]) {
				vols[item.volume] += item.size;
			} else {
				vols[item.volume] = item.size;
			}
		});

		console.log(vols, typeof(vols), typeof(pc));

		Object.keys(vols).forEach( (key) => {
			vols[key] += 'B';
		})

		res.json(vols);
	} else {
		notFound(res);
	}
});

app.get('/task3A/:lvl1', (req, res) => {
	const p = req.params;
	if (pc[p.lvl1] !== undefined) {
		res.json(pc[p.lvl1]);
	} else {
		notFound(res);
	}
});

app.get('/task3A/:lvl1/:lvl2', (req, res) => {
	const p = req.params;
	if (
		p.lvl2 !== 'length' &&
		pc[p.lvl1] !== undefined &&
		pc[p.lvl1][p.lvl2] !== undefined
	) {
		res.json(pc[p.lvl1][p.lvl2]);
	} else {
		notFound(res);
	}
});

app.get('/task3A/:lvl1/:lvl2/:lvl3', (req, res) => {
	const p = req.params;
	if (p.lvl3 !== 'length' &&
		pc[p.lvl1] !== undefined &&
		pc[p.lvl1][p.lvl2] !== undefined &&
		pc[p.lvl1][p.lvl2][p.lvl3] !== undefined
	) {
		res.json(pc[p.lvl1][p.lvl2][p.lvl3]);
	} else {
		notFound(res);
	}
});





app.get('/task2C', (req, res) => {
	const url = req.query.username;
	res.send(canonize(url));
});


// Преобразовываем ФИО
app.get('/fio', (req, res) => {
	let result = getShortFullName(req.query.fullname);
	console.log(req.query.fullname);
	console.log(result);
	res.send(result);
});


// Суммируем числа
app.get('/sum', (req, res) => {
	let result = '0';
	if (!isNumeric(req.query.a) && !isNumeric(req.query.b)) {
		result = '0';
	} else if (isNumeric(req.query.a) && !isNumeric(req.query.b)) {
		result = req.query.a;
	} else if (!isNumeric(req.query.a) && isNumeric(req.query.b)) {
		result = req.query.b;
	} else if (isNumeric(req.query.a) && isNumeric(req.query.b)) {
		result = String(parseFloat(req.query.a) + parseFloat(req.query.b));
	}
	console.log(result);
	res.send(result);
});


// Работаем с покемонами
const baseURl = 'https://pokeapi.co/api/v2';
const pokemonFields = ['id', 'name', 'base_experience', 'height', 'is_default', 'order', 'weight'];

async function getPokemons(url, i = 0) {
	console.log('getPokemons ', url, i);
	const response = await fetch(url);
	const page = await response.json();
	const pokemons = page.results;
	if (__DEV__ && i > 0) {
		return pokemons;
	}
	if (page.next) {
		const pokemons2 = await getPokemons(page.next, i + 1);
		return [
			...pokemons,
			...pokemons2
		]
	}
	return pokemons;
}

async function getPokemon(url) {
	console.log('getPokemon ', url);
	const response = await fetch(url);
	const pokemon = await response.json();

	return pokemon;
}

app.get('/', async(req, res) => {
	try {
		const pokemonsUrl = `${baseURl}/pokemon`;
		const pokemonsInfo = await getPokemons(pokemonsUrl);
		const pokemonsPromise = pokemonsInfo.slice(0, 3).map(info => {
			return getPokemon(info.url);
		});
		const pokemonsFull = await Promise.all(pokemonsPromise);
		const pokemons = pokemonsFull.map(pokemon => {
			return _.pick(pokemon, pokemonFields);
		});
		const sortPokemons = _.sortBy(pokemons, pokemon => -pokemon.weight);
		return res.json({sortPokemons});
	} catch (err) {
		console.log(err);
		res.json({err});
	}
});


//Получаем имя пользователя из URL
app.get('/canonize', (req, res) => {
	const username = canonize(req.query.url);
	res.json({
			url: req.query.url,
			username
		}
	);
});


app.listen(3000, function () {
	console.log('Example app listening on port 3000!');
});

