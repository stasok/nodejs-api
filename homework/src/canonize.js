export default function canonize(url) {
    const reg = new RegExp('@?(https?:)?(\/\/)?(www.)?(([a-zA-Z]*)[^\/]*\/)?(@)?([a-zA-Z0-9 (?._)]*)', 'i');
    const username = url.match(reg);
    return '@' + username[username.length - 1];
}