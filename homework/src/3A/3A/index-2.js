const app = require('express')();
const cors = require('cors')();
const fetch = require('node-fetch');
let pc;

getPc();

app.use(cors);

app.get(/.+/, (req,res) => {
	//берём запрос, убираем из него первый слэш
	let path = req.path.slice(1);
	if(path=='') res.json(pc);
	else if(path=='volumes') res.json(getVolumes());
	else {
		path=path.split('/');
		sendComponent(path, res);
	}
});

app.listen(80);

function sendComponent(path,res) {
	let component = pc;
	try {
		path.forEach(el => {
			if(el in component) component=component[el];
			else throw new Error('Not found');
		});
		res.json(component);
	}
	catch(e) {
		res.status(404).send('Not found');
	}

}

function getVolumes() {
	const volumes={};
	pc.hdd.forEach(el => {
		if(volumes[el.volume]) volumes[el.volume]+=el.size;
		else volumes[el.volume]=el.size;
	});
	for(i in volumes) volumes[i]+='B';
	return volumes;
}

async function getPc() {
	const pcUrl='https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
	return await fetch(pcUrl).then(async (res) => pc = await res.json())
		.catch(e => console.log(e));
}