import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';

const app = express();
const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
	.then(async (res) => {
		pc = await res.json();
	})
	.catch((err) => {
		console.log('Something goes wrong:', err);
	});

app.use(cors());

app.get('/task3a/', (req, res) => {
	res.status(200);
	res.json(pc);
});

app.get('/task3a/volumes', (req, res) => {
	const c = Number(pc.hdd[0].size) + Number(pc.hdd[2].size);
	const d = pc.hdd[1].size;
	res.status(200);
	res.json({
		'C:': c + 'B',
		'D:': d + 'B',
	});
});

app.get('/task3a/:element/:part/:det', (req, res) => {
	const elem = req.params.element;
	let part = req.params.part;
	const det = req.params.det;
	if (pc[elem] !== undefined && pc[elem][part] !== undefined && pc[elem][part][det] !== undefined) {
		res.status(200);
		if (!isNaN(Number(part))) {
			part = Number(part);
		}
		res.json(pc[elem][part][det]);
	} else {
		res.status(404);
		res.send('Not found');
	}
});

app.get('/task3a/:element/:part', (req, res) => {
	const elem = req.params.element;
	let part = req.params.part;
	if (pc[elem] !== undefined && pc[elem][part] !== undefined) {
		res.status(200);
		if (!isNaN(Number(part))) {
			part = Number(part);
		}
		res.json(pc[elem][part]);
	} else {
		res.status(404);
		res.send('Not found');
	}
});

app.get('/task3a/:element', (req, res) => {
	const elem = req.params.element;
	if (pc[elem] !== undefined) {
		res.status(200);
		res.json(pc[req.params.element]);
	} else {
		res.status(404);
		res.send('Not found');
	}
});

app.listen(3000, () => {
	console.log('Example app listening on port 3000!');
});
