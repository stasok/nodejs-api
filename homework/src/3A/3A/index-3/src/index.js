const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const getPcJson = require('./getPcJson');
const allowCrossDomain = require('./skillBranchCORS');

const app = express();

app.use(allowCrossDomain);
app.use(morgan('tiny'));

async function preloadPcInfo() {
  const pcInfo = await getPcJson();
  return pcInfo;
}

preloadPcInfo().then((pcInfo) => {
  const filePath = `./jsons_bak/pc_${Math.random().toString().slice(3, 8)}.json`;
  fs.writeFileSync(filePath, JSON.stringify(pcInfo, '', 2));

  const ERROR_MESSAGE_404 = 'Not found';

  const sendResponseIfPropExist = (obj, prop, res) => {
    if (obj.hasOwnProperty(prop)) {
      res.json(obj[prop]);
    } else {
      res.status(404).send(ERROR_MESSAGE_404);
    }
  };

  app.get('/', (req, res) => {
    res.json(pcInfo);
  });

  app.get('/volumes', (req, res) => {
    const volumeSizes = pcInfo.hdd.reduce((sizes, vol) => {
      const letter = vol.volume;
      if (sizes[letter] === undefined) sizes[letter] = 0;
      sizes[letter] += +vol.size;
      return sizes;
    }, {});

    for (const letter in volumeSizes) {
      volumeSizes[letter] = `${volumeSizes[letter]}B`;
    }

    res.json(volumeSizes);
  });

  app.get('/:prop', (req, res) => {
    sendResponseIfPropExist(pcInfo, req.params.prop, res);
  });

  app.get('/board/:prop', (req, res) => {
    sendResponseIfPropExist(pcInfo.board, req.params.prop, res);
  });

  app.get('/board/cpu/:prop', (req, res) => {
    sendResponseIfPropExist(pcInfo.board.cpu, req.params.prop, res);
  });

  app.get('/ram/:prop', (req, res) => {
    sendResponseIfPropExist(pcInfo.ram, req.params.prop, res);
  });

  app.get('/hdd/:index', (req, res) => {
    const index = req.params.index;
    if (isNaN(+index)) {
      return res.status(404).send(ERROR_MESSAGE_404);
    }
    sendResponseIfPropExist(pcInfo.hdd, req.params.index, res);
  });

  app.get('/hdd/:index/:prop', (req, res) => {
    const index = req.params.index;
    if (isNaN(+index) || !pcInfo.hdd.hasOwnProperty(index)) {
      return res.status(404).send(ERROR_MESSAGE_404);
    }
    return sendResponseIfPropExist(pcInfo.hdd[index], req.params.prop, res);
  });

  app.get('*', (req, res) => {
    res.status(404).send(ERROR_MESSAGE_404);
  });

  app.listen(3000, () => console.log('Server runing on 3000 port!'));
});
