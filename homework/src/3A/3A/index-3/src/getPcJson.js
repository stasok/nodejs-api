const fetch = require('isomorphic-fetch');

const url = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

async function getPcJson() {
  const response = await fetch(url);
  const pc = await response.json();

  return pc;
}

module.exports = getPcJson;
