'use strict';
var express = require('express');
var cors = require('cors');
var fetch = require('fetch');

var app = express();
app.use(cors());

var testcount = 0;




app.get('/volumes', function (req, res) {
	testcount++;
	console.log('testcount: ['+testcount+'] -----------------------------------');
	console.log(req.originalUrl);
	console.log(req.params);
	let v = {};
	for(let key in pc.hdd){
		const d = pc.hdd[key].volume;
		if( v[d] ){
			v[d]+=pc.hdd[key].size;
		}else{
			v[d]=pc.hdd[key].size;
		}
	}
	for(let key in v){
		v[key] = v[key]+'B';
	}
	console.log(v);
	res.json(v);
});

app.get(/^(?:\/)?(.*?)(?:\/)?$/, function (req, res) {
	testcount++;
	console.log('testcount: ['+testcount+'] -----------------------------------');
	console.log(req.originalUrl);
	console.log(req.params);
	const result = getKeyInJson(req.params[0],pc)
	console.log(result);
	if(result == 'Not found'){
		res.status(404).send(result);
	}else{
		res.json(JSON.parse(result));
	}
});

app.listen(3000, function () {
	console.log('Example app listening on port 3000!');
});





const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
let pc = {};

fetch.fetchUrl(pcUrl, function(err, meta, body){
    pc = JSON.parse(body);
    console.log('Все загружено. Можно пускать тесты');

});

function getKeyInJson(path,json){
	let result = 'Not found';

	if(!path)
		return JSON.stringify(pc);

	if(typeof path === "string")
		path = path.split(/\//);
	const key = path.shift();

	if(!(key in json) )
		return result;

	if(path.length){
		result = getKeyInJson(path,json[key]);
	}else{
		result = JSON.stringify(json[key]);
	}

	return result;
}
