import canonize from './canonize'

const array = [
    'https://vk.com/igor.suvorov',
    'https://vk.ru/igor.suvorov',
    'https://twittedsfsdr.cewrom/suvorovigor5',
    'https://telegram.me/skillbranch',
    '@skillbranch',
    'https://vk.com/skillbranch?w=wall-117903599_1076'
];

array.forEach((url) => {
    const username = canonize(url);

    console.log(username);
});
