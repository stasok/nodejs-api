import mongoose from 'mongoose';
import _ from 'lodash';
const { Schema } = mongoose;

const UserSchema = new Schema({
	name: {
		type: String,
		required: true,
	},
}, {
	timestamp: true
});

UserSchema.methods.toJSON = function () { // methods.toJSON происходит например при отдаче его браузеру
	return _.pick(this, ['name']); // берёт только name из текущего объекта
};

// для примера, если мы хотим например при отладке видеть в консоли не весь объект
// UserSchema.methods.toObject = function () {
// 	return {
// 		name: 'prefix_' + this.name,
// 	};
// };

export default mongoose.model('User', UserSchema);


/*

{
"user": {
	"name": "sssssss"
},
"pets": [
	{
		"name": "qqqqqqq",
		"type": "cat"
	},
 {
 "name": "nnnnnnnnn",
 "type": "dog"
 },
]
}

* */